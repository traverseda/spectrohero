#!/usr/bin/env python3

import pyaudio
import struct
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore
import signal
import pyqtgraph.opengl as gl
import numpy as np

FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 22050
#CHUNK = 512
CHUNK = 2056
OFFSET=0
# how much to scale down the volume
playback_SCALE=50000
micinput_SCALE=50000
DECAY=1

# looks like an octave is about -5 to -2
# * with chunk 2056, viewport 64 rate 22050
# so 3 units... so lets go -8 -5 -2 1 4 7

#VIEWPORT_height = 256
VIEWPORT_height = 64
VIEWPORT_width = 256

freq=np.fft.fftfreq(CHUNK,1/RATE)[1:VIEWPORT_height]
#freq=np.fft.fftfreq(CHUNK,1/RATE)[0:64]
#freq=np.fft.fftfreq(CHUNK,1/RATE)[1:2056]

def sigint_handler(*args):
    """sigint_Handler: Kill main loop if Ctrl-C is caught"""
    app.quit()

def update():
    """ Obtain next chunk of audio, compute FFT, update the waterfall display
          Called periodically using a timer"""
    global spectrographs
    for spectrograph in spectrographs:
        spectrograph.update()


app = QtGui.QApplication([])
audio = pyaudio.PyAudio()


class Window(QtGui.QMainWindow):
    """Main Window."""

    _defaultCamera = {'fov': 60,
            'rotation': QtGui.QQuaternion(1.0, 0.0, 0.0, 0.0),
            'azimuth': 180.0, 'center': QtGui.QVector3D(0.0, 0.0, 0.0),
            'elevation': 25.0, 'distance': 25.0}

    def __init__(self, parent=None):
        """Initializer."""
        super().__init__(parent)
        self.settings = QtCore.QSettings( 'OutsideContextSolutions', 'SpectroHero')     
        self.setWindowTitle("SpectroHero")
        self.resize(self.settings.value("size", QtCore.QSize(270, 225)))
        self.move(self.settings.value("pos", QtCore.QPoint(50, 50)))
        self.centralWidget = gl.GLViewWidget()
        self.setCentralWidget(self.centralWidget)
        self._createActions()
        self._createMenuBar()

    def resetCamera(self):
        print("reset camera")
        cameraParams = self._defaultCamera
        try:
            del cameraParams['rotation']# Can't set up both rotation and other values...
        except KeyError:
            pass
        win.setCameraParams(**cameraParams)

    def _createActions(self):
        self.cameraResetAction = QtGui.QAction("&Reset Camera", self)
        self.cameraResetAction.triggered.connect(self.resetCamera)

    def _createMenuBar(self):
        menuBar = self.menuBar()
        menuBar.addAction(self.cameraResetAction)


        inputMenu = QtGui.QMenu("&Inputs", self)
        menuBar.addMenu(inputMenu)
        topSourceMenu = QtGui.QMenu("&Top source", self)
        inputMenu.addMenu(topSourceMenu)
        bottomSourceMenu = QtGui.QMenu("&Bottom source", self)
        inputMenu.addMenu(bottomSourceMenu)

    def closeEvent(self, e):
        # Write window size and position to config file
        self.settings.setValue("size", self.size())
        self.settings.setValue("pos", self.pos())
        self.settings.setValue("3DCamera",self.centralWidget.cameraParams())
        e.accept()



mainWindow=Window()

mainWindow.show()
mainWindow.setWindowTitle('Waterfall')
win=mainWindow.centralWidget
mainWindow.resetCamera()


# all "input" need "getNextData" which returns a tuple of the next data.
# this could maybe be better represented as an iterator or some such.

class MicInput():

    def __init__(self):
        self.stream = audio.open(format=FORMAT,
                    channels=CHANNELS,
                    rate=RATE, input=True,
                    frames_per_buffer=CHUNK)
        self.FORMATSTR = "@%ih"%CHUNK

    def getNextData(self):
        # the microphone stream
        return( struct.unpack(self.FORMATSTR, self.stream.read(CHUNK)) )
    def stop():
        self.stream.stop_stream()
        self.stream.close()

class WavInput():
    def __init__(self,
            flpath="media/eqt-major-sc.wav",
            playback=True):
        # Thanks stanford for the recording of a c major scale.
        # Your musical web pages are always the best.
        # https://ccrma.stanford.edu/~mromaine/220a/fp/sound-examples.html
        # second only to michigan tech of course

        import wave
        self.wav = wave.open(flpath, 'rb')
        wf = self.wav
        if playback:
            self.playbackStream = audio.open(format=audio.get_format_from_width(wf.getsampwidth()),
                channels=wf.getnchannels(),
                rate=wf.getframerate(),
                output=True)
        else:
            self.playbackStream = None
    def getNextData(self):

        wf = self.wav

        rawdat = wf.readframes(CHUNK)
        if not rawdat:
            wf.rewind()
            rawdat = wf.readframes(CHUNK)
        self.playbackStream.write(rawdat)
        # ehh I don't actually know what format we are getting from the wav
        # or if it's really what the fft wants or what we get from example mic stream
        # so I should maybe do something like:
        #dat4fft = struct.unpack(FORMATSTR,rawdat)
        # but anyway that doesn't work so it's not that. It's it requires reading and understanding.
        # but this does seem to work, so maybe it's right:
        dat4fft = tuple( int(x) for x in rawdat )
        return( dat4fft )
    def stop():
        if self.playbackStream:
            self.playbackStream.stop_stream()
            self.playbackStream.close()


class Spectrograph():
    def __init__(self,
            stream,
            x1,y1,x2,y2,
            volume_scaling,
            resolution_x = 64,
            resolution_y = 64,
            ):

        self.stream = stream

        self.z = np.zeros((resolution_x,resolution_y))
        self.x = np.linspace(x1,x2, resolution_x).reshape(1,resolution_x)
        self.y = np.linspace(y1,y2, resolution_y).reshape(1,resolution_y)


        self._volume_scaling = volume_scaling
        self.deEmphasis = (1-np.exp(-(np.linspace(0,8,VIEWPORT_width+1) * DECAY)))/self._volume_scaling

        self.plot = gl.GLSurfacePlotItem(x=self.x[0,:], y = self.y[0,:],
        #self.plot = gl.GLSurfacePlotItem(x=self.x, y = self.y,
                                shader='heightColor',
                                computeNormals=False, smooth=True)

        self.resolution_x = resolution_x
        self.resolution_y = resolution_y

    def setColor(self, colors):
        self.plot.shader()['colorMap'] = colors

    def addToWindow(self, window):
        window.addItem(self.plot)

    def update(self):
        data = self.stream.getNextData()

        data = np.fft.fft( data )
        # what is the [:indexing] in the following doing? I just copy pasted it...
        data =abs(data[:self.resolution_y]) * self.deEmphasis[:self.resolution_y]
        self.z=np.vstack((data[:self.resolution_y],self.z[:-1])) # this last [:-1] is shaving off the last data aka fifoing the stream
        self.plot.setData(z=self.z)
    def stop(self):
        self.stream.stop()



## add the two graphs

### microphone input graph
micgraph = Spectrograph(
        MicInput(),
        0,-8, 8,8,
        50000,
        )
micgraph.setColor( np.linspace(0.,8,100) )
micgraph.addToWindow(win)

### audio recording playback graph
playbackgraph = Spectrograph(
        WavInput(),
        -8,-8, 0,8,
        50000,
        )
playbackgraph.setColor(np.array([0.2, 2, 0.5, 0.2, 1, 1, 0.2, 0, 2]))
playbackgraph.addToWindow(win)


spectrographs = [ micgraph, playbackgraph ]



## Add a grid to the view

gridxy = gl.GLGridItem()
gridxy.scale(1,1,1)
win.addItem(gridxy)


## stuff for drawing scales on

def addLineAtGLPos(pos,col=(0.9,0.9,0.9,1)):
    line = gl.GLLinePlotItem()
    #line.setData(pos=np.array([0,0,0,1,1,1]))
    line.setData(color=col)
    line.setData(width=1.0)
    line.setData(pos=np.array([[-10,pos,.1],[10,pos,.1]]))
    win.addItem(line)

def addWhiteLineAtGLPos(pos):
    addLineAtGLPos(pos)
def addBlackLineAtGLPos(pos):
    addLineAtGLPos(pos,col=(0.4,0.4,0.4,1))


musicScale = ("T","t","s","t","t","t","s")

def addScale(
        startGLPos=-8,
        endGLPos=8,
        numOctaves=2
        ):
    stepGLSize = (endGLPos - startGLPos) / (12 * numOctaves)
    pos = startGLPos
    for octave in range(numOctaves):
        for step in musicScale:
            if step=="T":
                addLineAtGLPos(pos,col=(1,0,0,1))
                pos+=stepGLSize
                addBlackLineAtGLPos(pos)
                pos+=stepGLSize
            elif step=="t":
                addWhiteLineAtGLPos(pos)
                pos+=stepGLSize
                addBlackLineAtGLPos(pos)
                pos+=stepGLSize
            elif step=="s":
                addWhiteLineAtGLPos(pos)
                pos+=stepGLSize
            else:
                print(f"I don't know what kind of step {step} is. Is it a tone? A semitone? Who knows?")
    # add the final C
    addLineAtGLPos(pos,col=(1,0,0,1))

def addLogScale(
        startGLPos=-8,
        endGLPos=8,
        numOctaves=2
        ):
    # np.log
    # np.exp
    offset = 1-startGLPos
    stepGLSize = (np.log(endGLPos+offset) - np.log(startGLPos+offset)) / (12 * numOctaves)
    pos = np.log(startGLPos+offset)
    for octave in range(numOctaves):
        for step in musicScale:
            if step=="T":
                addLineAtGLPos(np.exp(pos)-offset,col=(1,0,0,1))
                pos+=stepGLSize
                addBlackLineAtGLPos(np.exp(pos)-offset)
                pos+=stepGLSize
            elif step=="t":
                addWhiteLineAtGLPos(np.exp(pos)-offset)
                pos+=stepGLSize
                addBlackLineAtGLPos(np.exp(pos)-offset)
                pos+=stepGLSize
            elif step=="s":
                addWhiteLineAtGLPos(np.exp(pos)-offset)
                pos+=stepGLSize
            else:
                print(f"I don't know what kind of step {step} is. Is it a tone? A semitone? Who knows?")
    # add the final C
    addLineAtGLPos(np.exp(pos)-offset,col=(1,0,0,1))

#addScale( -8, 7, 5)
#addLogScale(-5,-1.8,1)
addScale(-5,-1.8,1)


#gridzy = gl.GLGridItem()
#gridzy.scale(1,1,1)
#gridzy.rotate(90, 1, 0, 0)
#win.addItem(gridzy)
#gridzy.translate(0,-10,10)
timer = QtCore.QTimer()
timer.timeout.connect(update)
#timer.start(10)
# as you might guess, this calls update once every n milliseconds
timer.start(1)
#timer.start(0.1)

def main():
    import sys
    signal.signal(signal.SIGINT, sigint_handler)

    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()

    print ("\nCleaning up...")
    for spectrograph in spectrographs:
        spectrograph.stop()
    audio.terminate()


if __name__ == '__main__':
    main()
